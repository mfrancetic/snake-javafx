import de.awacademy.model.Snake;
import de.awacademy.model.SnakeSquare;
import de.awacademy.model.UserInputEnum;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class SnakeTest {

    @Test
    public void testSnakeSpeed() {
        Snake snake = new Snake();
        assertEquals(snake.getSpeed(), 10);
    }

    @Test
    public void testSnakeLength() {
        Snake snake = new Snake();
        assertEquals(snake.getLength(), 3);
    }
}