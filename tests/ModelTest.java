import de.awacademy.model.Model;
import org.junit.Test;

import static org.junit.Assert.*;

public class ModelTest {

    @Test
    public void testScore() {
        Model model = new Model();
        assertEquals(model.getScore(), 0);
    }

    @Test
    public void testLength() {
        Model model = new Model();
        assertEquals(model.getLength(), 3);
    }

    @Test
    public void testRestartGame() {
        Model model = new Model();
        assertEquals(model.getScore(), 0);
        assertEquals(model.getLength(), 3);
    }
}
