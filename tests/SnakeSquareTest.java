import static org.junit.Assert.*;

import de.awacademy.model.SnakeSquare;
import org.junit.Test;

public class SnakeSquareTest {

    @Test
    public void testSnakeSquareX(){
        SnakeSquare snakeSquare = new SnakeSquare(10, 10);
        assertEquals(snakeSquare.getX(), 10);
    }

    @Test
    public void testSnakeSquareY(){
        SnakeSquare snakeSquare = new SnakeSquare(10, 10);
        assertEquals(snakeSquare.getY(), 10);
    }
}
