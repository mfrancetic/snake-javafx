package de.awacademy;

import de.awacademy.model.Model;
import javafx.animation.AnimationTimer;

public class Timer extends AnimationTimer {

    private static Graphics graphics;
    private static Model model;
    private long lastTick;

    public Timer(Graphics graphics, Model model) {
        Timer.graphics = graphics;
        Timer.model = model;
    }

    /**
     * The method handles is called in each frame while the Timer is active.
     * It defines the speed of the game, by setting the number of times the game is redrawn in a
     * second (with the variable Snake.speed)
     *
     * @param nowNano is the timestamp of the current frame given in nanoseconds
     */
    @Override
    public void handle(long nowNano) {
        /* the game starts with the tick value of 0, and it then sets it to now
        the model is updated */
        if (lastTick == 0) {
            lastTick = nowNano;
            model.update();
            return;
        }

        /* define the speed of one tick and the speed of the game (if speed is 5, there are 5
         ticks in second
         speed - the game is redrawn once per second (the higher the number, faster the snake)
        the model is updated */
        if (nowNano - lastTick > 1000000000 / Model.getSnake().getSpeed()) {
            lastTick = nowNano;
            model.update();
        }
    }

    public static Graphics getGraphics() {
        return graphics;
    }
}