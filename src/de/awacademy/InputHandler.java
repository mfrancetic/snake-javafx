package de.awacademy;

import de.awacademy.model.Model;
import de.awacademy.model.UserInputEnum;
import javafx.scene.input.KeyCode;

public class InputHandler {

    public static UserInputEnum direction = UserInputEnum.left;

    public InputHandler() {
    }

    /**
     * The method listens to the keys pressed, and defines the UserInputEnum accordingly
     * (up, down, left, right, restart)
     *
     * @param keycode is the key that the user has pressed
     */
    public void onKeyPressed(KeyCode keycode) {
        if (keycode == KeyCode.UP || keycode == KeyCode.W) {
            direction = UserInputEnum.up;
        }
        if (keycode == KeyCode.DOWN || keycode == KeyCode.S) {
            direction = UserInputEnum.down;
        }
        if (keycode == KeyCode.LEFT || keycode == KeyCode.A) {
            direction = UserInputEnum.left;
        }
        if (keycode == KeyCode.RIGHT || keycode == KeyCode.D) {
            direction = UserInputEnum.right;
        }
        if (keycode == KeyCode.SPACE) {
            // if the game is over or it hasn't yet started, set the direction to restart
            if (Model.isGameOver() || !Model.isGameStarted()) {
                direction = UserInputEnum.restart;
            } else {
                // if the game is not over and has started, continue in the same direction
                direction = Model.getSnake().getUserInputEnum();
            }
        }
    }
}