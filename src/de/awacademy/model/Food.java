package de.awacademy.model;

import java.util.Random;

public class Food {

    private int x;
    private int y;
    private Random random;
    private String foodImageName;
    private int foodColorInt = 0;

    public Food() {
        generateRandomCoordinates();
        for (SnakeSquare snakeSquare : Model.getSnake().getSnakeBody()) {
            /* in case the food is located on the same location as a part of a snake,
            or on the y coordinate of the application bar, repeat the generation of the random coordinates */
            if (snakeSquare.getX() == x && snakeSquare.getY() == y || y < Model.APPLICATION_BAR_Y) {
                generateRandomCoordinates();
            }
        }
        foodColorInt = random.nextInt(5);
        setFoodColor(foodColorInt);
    }

    /**
     * The method defines the color of the food based on a randomly generated integer
     *
     * @param foodColorInt is the randomly generated integer (from 0 to 5), that will
     *                     define the color of the food
     */
    public void setFoodColor(int foodColorInt) {
        switch (foodColorInt) {
            case 0:
                foodImageName = "bug.jpg";
                break;
            case 1:
                foodImageName = "bug2.png";
                break;
            case 2:
                foodImageName = "bug3.png";
                break;
            case 3:
                foodImageName = "bug4.png";
                break;
            case 4:
                foodImageName = "bug5.png";
                break;
        }
    }

    /**
     * Generate random x and y coordinates for the location of food
     */
    private void generateRandomCoordinates() {
        random = new Random();
        x = random.nextInt(Model.WIDTH);
        y = random.nextInt(Model.HEIGHT);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public String getFoodImageName() {
        return foodImageName;
    }

    public int getFoodColorInt() {
        return foodColorInt;
    }
}