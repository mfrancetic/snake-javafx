package de.awacademy.model;

import de.awacademy.Graphics;
import de.awacademy.InputHandler;
import de.awacademy.Timer;

import java.util.List;

public class Model {

    private static Snake snake;
    private Food food;
    private List<SnakeSquare> snakeBody;
    private int length = 3;
    private int score = 0;
    private static boolean isGameOver = false;
    private static boolean isGameStarted = false;
    private static boolean hasEatenFood = false;
    private static boolean isSoundPlaying = false;

    public static final int WIDTH = 20;
    public static final int HEIGHT = 20;
    public static final int SQUARE_SIZE = 25;
    public static final int APPLICATION_BAR_Y = 2;

    public Model() {
        snake = new Snake();
        food = new Food();
        snakeBody = snake.getSnakeBody();
    }

    public static Snake getSnake() {
        return snake;
    }

    /**
     * The method updates the model, by setting snake coordinates and animating it, checking if the snake eats
     * the food and collides.
     * It calls the graphics.draw() method afterwards
     */
    public void update() {
        if (!isGameOver && isGameStarted) {
            setSnakeCoordinates();
            animateSnake();
            checkIfSnakeEatsFood();
            checkSnakeCollisionAgainstItself();
        }
        checkGameState();
        Graphics graphics = Timer.getGraphics();
        graphics.draw();
    }

    public Food getFood() {
        return food;
    }

    /**
     * The method checks if the snake's head is at the same position as the food.
     * If so, the length increases (the number of snake squares in increased by 1 and positioned at the end of the snake).
     * The speed and score are increased.
     */
    private void checkIfSnakeEatsFood() {
        int speed = snake.getSpeed();
        if (food.getX() == snakeBody.get(0).getX() && food.getY() == snakeBody.get(0).getY()) {
            snakeBody.add(new SnakeSquare(-1, -1));
            food = new Food();
            snake.setSpeed(speed += 5);
            snake.setLength(++length);
            score = speed * length;
            hasEatenFood = true;
        } else {
            hasEatenFood = false;
        }
    }

    /**
     * Check if the snake collides with itself
     * (if the snake's head coordinates are the same as the coordinates of the body).
     * If so, set the isGameOver boolean to true.
     */
    private void checkSnakeCollisionAgainstItself() {
        for (int i = 3; i < snakeBody.size(); i++) {
            if (snakeBody.get(0).getX() == snakeBody.get(i).getX() && snakeBody.get(0).getY() == snakeBody.get(i).getY()) {
                isGameOver = true;
                return;
            }
        }
    }

    public static boolean isGameOver() {
        return isGameOver;
    }

    /**
     * The method restarts the game
     * The score and speed are reset, new Snake and Food objects are created, the direction is set to left,
     * and the isGameOver boolean is set to false.
     */
    public void restartGame() {
        score = 0;
        length = 3;
        snake = new Snake();
        snake.setSpeed(10);
        food = new Food();
        InputHandler.direction = UserInputEnum.left;
        isGameOver = false;
        isGameStarted = true;
    }

    /**
     * The method sets the coordinates of each snake square, so that each snake square is positioned after the
     * previous one
     */
    private void setSnakeCoordinates() {
        for (int i = snake.getSnakeBody().size() - 1; i >= 1; i--) {
            snakeBody.get(i).setX(snakeBody.get(i - 1).getX());
            snakeBody.get(i).setY(snakeBody.get(i - 1).getY());
        }
    }

    /**
     * The method defines the moveUp movement (decreases the y coordinate of the snake's head)
     * The snake's direction is updated to UserInputEnum.up
     *
     * @param y is the y coordinate of the snake's head
     */
    private void moveUp(int y) {
        snakeBody.get(0).setY(--y);
        snake.setUserInputEnum(UserInputEnum.up);
    }

    /**
     * The method defines the moveDown movement (increases the y coordinate of the snake's head)
     * The snake's direction is updated to UserInputEnum.down
     *
     * @param y is the y coordinate of the snake's head
     */
    private void moveDown(int y) {
        snakeBody.get(0).setY(++y);
        snake.setUserInputEnum(UserInputEnum.down);
    }

    /**
     * The method defines the moveLeft movement (decreases the x coordinate of the snake's head)
     * The snake's direction is updated to UserInputEnum.left
     *
     * @param x is the x coordinate of the snake's head
     */
    private void moveLeft(int x) {
        snakeBody.get(0).setX(--x);
        snake.setUserInputEnum(UserInputEnum.left);
    }

    /**
     * The method defines the moveRight movement (increases the x coordinate of the snake's head)
     * The snake's direction is updated to UserInputEnum.right
     *
     * @param x is the x coordinate of the snake's head
     */
    private void moveRight(int x) {
        snakeBody.get(0).setX(++x);
        snake.setUserInputEnum(UserInputEnum.right);
    }

    /**
     * The method animates the snake accordingly, based on the InputHandler's direction (up, down, left, right, restart)
     */
    public void animateSnake() {
        snakeBody = snake.getSnakeBody();
        int x = snakeBody.get(0).getX();
        int y = snakeBody.get(0).getY();
        switch (InputHandler.direction) {

            case up:
                /* if the snake's head is at the y position smaller than 2 (the position of the application bar),
                set boolean isGameOver to true */
                if (y <= APPLICATION_BAR_Y) {
                    isGameOver = true;
                    break;
                }
                // disable the possibility of the snake going backwards (from down to up)
                if (snake.getUserInputEnum() != UserInputEnum.down) {
                    moveUp(y);
                } else {
                    moveDown(y);
                }
                break;
            case down:
                // if the snake's head is at the y position larger than Model.HEIGTH - 1, set boolean isGameOver to true
                if (y >= Model.HEIGHT - 1) {
                    isGameOver = true;
                    break;
                }
                // disable the possibility of the snake going backwards (from up to down)
                if (snake.getUserInputEnum() != UserInputEnum.up) {
                    moveDown(y);
                } else {
                    moveUp(y);
                }
                break;
            case left:
                // if the snake's head is at the x position smaller than 0, set boolean isGameOver to true
                if (x <= 0) {
                    isGameOver = true;
                    break;
                }
                /* disable the possibility of the snake going backwards (from right to left) */
                if (snake.getUserInputEnum() != UserInputEnum.right) {
                    moveLeft(x);
                } else {
                    moveRight(x);
                }
                break;
            case right:
                /* if the snake's head is at the x position larger than MODEL.WIDTH -1,
                set boolean isGameOver to true */
                if (x >= Model.WIDTH - 1) {
                    isGameOver = true;
                    break;
                }
                // disable the possibility of the snake going backwards (from left to right)
                if (snake.getUserInputEnum() != UserInputEnum.left) {
                    moveRight(x);
                } else {
                    moveLeft(x);
                }
                break;
        }
    }

    /**
     * The method checks the game state.
     * In the event that the user has pushed the restart (space) button, if the game is over or it hasn't started yet,
     * restart it.
     * It also defines when the sound is playing according to the game state.
     */
    private void checkGameState() {
        if (!isGameOver && isGameStarted()) {
            isSoundPlaying = false;
        }
        if (InputHandler.direction == UserInputEnum.restart) {
            if (isGameOver || !isGameStarted()) {
                restartGame();
                isSoundPlaying = true;
            }
        }
    }

    public int getScore() {
        return score;
    }

    public static boolean isGameStarted() {
        return isGameStarted;
    }

    public static boolean hasEatenFood() {
        return hasEatenFood;
    }

    public static boolean isMusicPlaying() {
        return isSoundPlaying;
    }

    public int getLength() {
        return length;
    }
}