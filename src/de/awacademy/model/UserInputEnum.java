package de.awacademy.model;

/**
 * Enum for the user input (left, right, up, down, restart)
 */
public enum UserInputEnum {

    left, right, up, down, restart
}