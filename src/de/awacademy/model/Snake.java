package de.awacademy.model;

import java.util.ArrayList;
import java.util.List;

public class Snake {

    private int speed = 10;
    private UserInputEnum userInputEnum;
    private List<SnakeSquare> snakeBody = new ArrayList<>();
    private int length;

    public Snake() {
        for (int i = 0; i < 3; i++) {
            // position the snake on the middle of the playing field, with a length of 3 squares
            snakeBody.add(new SnakeSquare(Model.WIDTH / 2, Model.HEIGHT / 2));
            length = 3;
        }
    }

    public List<SnakeSquare> getSnakeBody() {
        return snakeBody;
    }

    public UserInputEnum getUserInputEnum() {
        return userInputEnum;
    }

    public void setUserInputEnum(UserInputEnum userInputEnum) {
        this.userInputEnum = userInputEnum;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}