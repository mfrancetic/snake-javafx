package de.awacademy;

import de.awacademy.model.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static de.awacademy.model.Model.SQUARE_SIZE;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    private Snake snake;
    private List<SnakeSquare> snakeBody;
    private FileInputStream inputStream;

    public Graphics(GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
    }

    public void draw() {
        // in case the game has not yet started, draw the background and welcome screen, and return from the method
        if (!Model.isGameStarted()) {
            drawBackground();
            drawWelcomeScreen();
            return;
        }
        // in case the game is over, draw the game over screen and return from the method
        if (Model.isGameOver()) {
            drawGameOverScreen();
            return;
        }
        setSound();
        drawBackground();
        drawBorders();

        snake = Model.getSnake();
        snakeBody = snake.getSnakeBody();

        drawSnake();
        drawApplicationToolbar();
        drawScore();
        drawLength();
        drawFood();
    }

    /**
     * The method sets the sound in the background
     */
    private void setSound() {
        // in case the snake has eaten the food or the game has started, set the sound
        if (Model.hasEatenFood() || Model.isMusicPlaying()) {
            String soundPath = "";
            if (Model.hasEatenFood()) {
                soundPath = "src\\de\\awacademy\\sounds\\eat_sound.mp3";
            }
            // in case the game has started, set the sound
            if (Model.isMusicPlaying()) {
                soundPath = "src\\de\\awacademy\\sounds\\game_started_sound.mp3";
            }
            Media sound = new Media(new File(soundPath).toURI().toString());
            MediaPlayer mediaPlayer = new MediaPlayer(sound);
            mediaPlayer.play();
        }
    }

    /**
     * The method draws food on the playing field (either with an image, or in case of an exception,
     * with a coloured oval)
     */
    private void drawFood() {
        Food food = model.getFood();
        Image image;
        try {
            inputStream = new FileInputStream("src\\de\\awacademy\\images\\" + food.getFoodImageName());
            image = new Image(inputStream);
            gc.drawImage(image, food.getX() * SQUARE_SIZE, food.getY() * SQUARE_SIZE, SQUARE_SIZE + 1, SQUARE_SIZE + 1);
        } catch (Exception e) {
            e.printStackTrace();
            int foodColorInt = model.getFood().getFoodColorInt();
            Color color = Color.ORANGE;
            switch (foodColorInt) {
                case 0:
                    color = Color.PURPLE;
                    break;
                case 1:
                    color = Color.BLUE;
                    break;
                case 2:
                    color = Color.YELLOW;
                    break;
                case 3:
                    color = Color.RED;
                    break;
                case 4:
                    color = Color.ORANGE;
                    break;
            }
            gc.setFill(color);
            gc.fillOval(food.getX() * SQUARE_SIZE, food.getY() * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE);
        }
    }

    /**
     * The method draws the application toolbar (sets the color, fills the rectangle, and displays the title of
     * the game)
     */
    private void drawApplicationToolbar() {
        gc.setFill(Color.GREEN);
        gc.fillRect(0, 0, Model.WIDTH * SQUARE_SIZE, 50);
        gc.setFill(Color.WHITE);
        gc.setFont(new Font("", 25));
        gc.fillText("THE SNAKE", 180, 35);
    }

    /**
     * The method draws score in the upper left corner of the application toolbar
     */
    private void drawScore() {
        gc.setFill(Color.LIGHTGREEN);
        gc.setFont(new Font("", 20));
        gc.fillText("Score: " + model.getScore(), 10, 30);
    }

    /**
     * The method draws the length of the snake (number of snake squares) in the upper right corner of the
     * application toolbar
     */
    private void drawLength() {
        int length = snake.getLength();
        gc.setFill(Color.LIGHTGREEN);
        gc.setFont(new Font("", 20));
        gc.fillText("Size: " + length, 420, 30);
    }

    /**
     * The method draws the game over screen (sets the "Game over" text and provides instructions on how to
     * restart the game using the "SPACE" key)
     */
    private void drawGameOverScreen() {
        gc.setFill(Color.LIGHTGREEN);
        gc.setFont(new Font("", 30));
        gc.fillText("GAME OVER", 170, 250);
        gc.setFill(Color.WHITE);
        gc.setFont(new Font("", 25));
        gc.fillText("Press \"SPACE\" to restart game", 90, 290);
    }

    /**
     * The method draws the snake on the playing field
     * The snake head is either represented with an image, or in case of an exception, with a green square
     * The body consists of green snake squares
     */
    private void drawSnake() {
        for (int i = 0; i < snakeBody.size(); i++) {
            if (i == 0) {
                try {
                    inputStream = new FileInputStream("src\\de\\awacademy\\images\\snake_head_green_.png");
                    Image image = new Image(inputStream);
                    gc.drawImage(image, snakeBody.get(i).getX() * SQUARE_SIZE, snakeBody.get(i).getY() *
                            SQUARE_SIZE, SQUARE_SIZE - 2, SQUARE_SIZE - 2);
                } catch (Exception e) {
                    e.printStackTrace();
                    drawSnakeBodySquares(i);
                }
            } else {
                drawSnakeBodySquares(i);
            }
        }
    }

    /**
     * The method draws the background of the game (clears the whole background, and then sets the color to black)
     */
    private void drawBackground() {
        gc.clearRect(0, 0, Model.WIDTH * SQUARE_SIZE, Model.HEIGHT * SQUARE_SIZE);
        try {
            inputStream = new FileInputStream("src\\de\\awacademy\\images\\background.jpg");
            Image image = new Image(inputStream);
            gc.drawImage(image, 0, 0, Model.WIDTH * SQUARE_SIZE, Model.HEIGHT * SQUARE_SIZE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            gc.setFill(Color.BLACK);
            gc.fillRect(0, 0, Model.WIDTH * SQUARE_SIZE, Model.HEIGHT * SQUARE_SIZE);
        }
    }

    /**
     * The method draws the borders on the left, right and bottom
     */
    private void drawBorders() {
        gc.setFill(Color.GREEN);
        gc.fillRect(0, 2, 5, Model.HEIGHT * SQUARE_SIZE);
        gc.setFill(Color.GREEN);
        gc.fillRect((Model.WIDTH * SQUARE_SIZE) - 4, 2, 5, Model.HEIGHT * SQUARE_SIZE);
        gc.setFill(Color.GREEN);
        gc.fillRect(0, (Model.HEIGHT * SQUARE_SIZE) - 4, Model.WIDTH * SQUARE_SIZE, 5);
    }

    /**
     * The method draws the body of the snake (sets the colors of each snake square, in the correct position - each
     * snake square after the previous one)
     *
     * @param i is the index of the snake square
     */
    private void drawSnakeBodySquares(int i) {
        gc.setFill(Color.LIGHTGREEN);
        gc.fillRect(snakeBody.get(i).getX() * SQUARE_SIZE, snakeBody.get(i).getY() * SQUARE_SIZE,
                SQUARE_SIZE - 1, SQUARE_SIZE - 1);
        gc.setFill(Color.FORESTGREEN);
        gc.fillRect(snakeBody.get(i).getX() * SQUARE_SIZE, snakeBody.get(i).getY() * SQUARE_SIZE,
                SQUARE_SIZE - 2, SQUARE_SIZE - 2);
    }

    /**
     * Method draws the welcome screen with the instructions for the game and how to start it
     */
    private void drawWelcomeScreen() {
        gc.setFill(Color.LIGHTGREEN);
        gc.setFont(new Font("", 30));
        gc.fillText("Welcome to The Snake game!", 60, 150);
        gc.setFont(new Font("", 20));
        gc.setFill(Color.WHITE);
        gc.fillText("Eat the food to grow, and try not to collide", 65, 190);
        gc.fillText("with the walls and yourself.", 140, 220);
        gc.fillText("Have fun!", 210, 250);
        gc.setFill(Color.LIGHTGREEN);
        gc.fillText("Press \"SPACE\" to start the game", 110, 280);
        try {
            inputStream = new FileInputStream("src\\de\\awacademy\\images\\snake_head_green_.png");
            Image image = new Image(inputStream);
            gc.drawImage(image, 220, 310, SQUARE_SIZE * 3, SQUARE_SIZE * 3);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}