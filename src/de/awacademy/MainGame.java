package de.awacademy;

import de.awacademy.model.Model;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

import static de.awacademy.model.Model.SQUARE_SIZE;

public class MainGame extends Application {

    @Override
    public void start(Stage stage) {

        // Create a new model, canvas and group
        Model model = new Model();
        Canvas canvas = new Canvas(Model.WIDTH * SQUARE_SIZE, Model.HEIGHT * SQUARE_SIZE);
        Group group = new Group();
        group.getChildren().add(canvas);

        // Create a new scene and set it to the stage
        Scene scene = new Scene(group);
        stage.setScene(scene);

        // Get the graphics context and pass it as a parameter to the Graphics constructor
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Graphics graphics = new Graphics(gc, model);

        // Create a new timer and start it
        Timer timer = new Timer(graphics, model);
        timer.start();

        InputHandler inputHandler = new InputHandler();

        // in case a key is pressed, pass the code of the event to the InputHandler's onKeyPressed method
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );

        stage.show();
    }
}