## Maja Francetic - Snake

Snake game app, where the player maneuvers a snake which grows in length when eating food, with the snake itself being a primary obstacle. 

The game is developed in Java programming language, while using the JavaFX SDK.